import javax.swing.*;

class TimerThread extends Thread {
	public void run() {
		try {
			for (int i = 60; i >= 0; i--) {
				Game2.Timer.setText("" + i);
				Thread.sleep(1000);
				if (Game2.Timer.getText().equals("0")) {
					Game2.f.setVisible(false);
					JOptionPane.showMessageDialog(Game2.f, "Game Over. Your Score is " + Game2.Score.getText().toString());
				}
			}
		} catch (Exception e) {
		}
	}
}
