import java.awt.*;

class BulletThread1 extends Thread {

	Button b1 = new Button();
	int y = 360;
	int x = 0;
	public void run() {
		try {
			x = Game2.Tank1.getX() + 20;
			for (y = 360; y > 0; y -= 10) {
				b1.setBounds(x, y, 10, 10);
				Game2.f.add(b1);
				Thread.sleep(20);
			}
		} catch (Exception e1) {
		}
	}
	
	public int getX() {
		return b1.getX();
	}
	
	public int getY() {
		return b1.getY();
	}
	
	public void changeColor() {
		x = 0;
		b1.setVisible(false);
	}
}