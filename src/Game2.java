import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

class Game2 implements WindowListener, MouseMotionListener, MouseListener {
	static Frame f;
	static Panel p;
	static Button Tank1, Tank2, Tank3;
	static Label Score, Timer, l1, l2;
	static BulletThread1 bt1;
	static BulletThread2 bt2;
	static BulletThread3 bt3;


	Game2() {
		f = new Frame("Game");
		f.addWindowListener(this);
		f.addMouseMotionListener(this);
		f.addMouseListener(this);

		Tank1 = new Button("Fire");
		Tank1.addMouseListener(this);
		Tank2 = new Button();
		Tank3 = new Button();
		p = new Panel();
		Score = new Label("0");
		Timer = new Label("60");
		l1 = new Label("SCORE");
		l2 = new Label("TIMER");
		f.add(p);
		f.add(Tank1);
		f.add(Tank2);
		f.add(Tank3);
		p.add(l1);
		p.add(Score);
		p.add(l2);
		p.add(Timer);
		f.setLayout(null);
		p.setBounds(0, 30, 400, 40);
		p.setBackground(new Color(228, 228, 228));
		l1.setBounds(10, 30, 100, 30);
		Score.setBounds(110, 30, 100, 30);
		l2.setBounds(210, 30, 100, 30);
		Timer.setBounds(300, 30, 100, 30);
		Tank1.setBounds(160, 350, 80, 50);
		Tank2.setBounds(50, 80, 50, 40);
		Tank3.setBounds(300, 80, 50, 40);
		l1.setFont(new Font("Arial", 1, 12));
		l2.setFont(new Font("Arial", 1, 12));
		Score.setFont(new Font("Arial", 1, 12));
		Timer.setFont(new Font("Arial", 1, 12));
		f.setVisible(true);
		f.setSize(400, 400);
		
		bt2 = new BulletThread2();
		bt2.start();
		bt3 = new BulletThread3();
		bt3.start();
		TimerThread t = new TimerThread();
		t.start();		
	}

	public static void main(String ar[]) {
		new Game2();
		
		int x1 = 0, x2 = 0, x3 = 0;
		int y1 = 0, y2 = 0, y3 = 0, y4 = 0, y5 = 0;
		boolean allow = true;
		int count = 0;
		while(allow){
			if(bt1!=null) {
				x1 = bt1.getX();
				y1 = bt1.getY();
			}
			
			x2 = bt2.getX();
			y2 = bt2.getY();
			
			x3 = bt3.getX();
			y3 = bt3.getY();
			if ((bt1!=null && bt2!=null) && (between(x2, x1)) && y2!=y4 && (y1 == y2)) {
				bt2.changeColor();
				bt1.changeColor();
				y4=y2;
				count++;
			} else if ((bt1!=null && bt3!=null) && (between(x3, x1)) && y3!=y5 && (y1 == y3)) {
				bt3.changeColor();
				bt1.changeColor();
				y5=y3;
				count++;
			}
			
			Game2.Score.setText(String.valueOf(count));
			
			if (Game2.Timer.getText().equals("0"))
				allow = false;
		}
		
	}
	
	public static boolean between(int i, int j) {
	    return ((i +5) >= j && (i-5) <= j);
	}

	public void windowClosing(WindowEvent e) {
		int a = JOptionPane.showConfirmDialog(f, "Quit Game?");
		if (a == 0)
			f.dispose();
	}

	public void windowClosed(WindowEvent e) {
	}

	public void windowIconified(WindowEvent e) {
	}

	public void windowDeiconified(WindowEvent e) {
	}

	public void windowOpened(WindowEvent e) {
	}

	public void windowActivated(WindowEvent e) {
	}

	public void windowDeactivated(WindowEvent e) {
	}

	public void mouseMoved(MouseEvent e) {
		int x = e.getX();
		Tank1.setBounds(x, 350, 80, 50);
	}

	public void mouseDragged(MouseEvent e) {
		
	}

	public void mousePressed(MouseEvent e) {
		
	}

	public void mouseReleased(MouseEvent e) {
		
	}

	public void mouseClicked(MouseEvent e) {
		bt1 = new BulletThread1();
		bt1.start();
	}

	public void mouseEntered(MouseEvent e) {
		
	}

	public void mouseExited(MouseEvent e) {
		
	}
}